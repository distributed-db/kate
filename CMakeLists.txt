cmake_minimum_required(VERSION 3.2)

project(kate VERSION 1.0.0)

include_directories(
    ${PROJECT_SOURCE_DIR}/log/include
)

set(SOURCE_FILES ${PROJECT_SOURCE_DIR}/log/src/hilog/log.cpp)

add_library(hilog STATIC ${SOURCE_FILES})
