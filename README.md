## Kate-ut
---

### 使用方式
> 本项目运行在`linux`或`window-WSL`环境

将本项目的`CMakeLists.txt`文件放在`distributeddatamgr_datamgr`目录下，然后执行以下命令。
首次执行`cmake ..`命令会下载、编译第三方依赖，后续修改代码后可在`build`目录直接执行`make`命令。

``` shell
cd distributeddatamgr_kv_store
mkdir -p build
cd build
cmake ..
make -j8
./distributed_ut --gtest_filter="xxx.xx"
```
