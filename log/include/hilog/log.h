#ifndef VIRTUAL_LOG_PRINTF
#define VIRTUAL_LOG_PRINTF

enum LogType {
    LOG_TYPE_MIN = 0,
    LOG_INIT = 1,
    LOG_CORE = 3,
    LOG_TYPE_MAX
};

namespace OHOS::HiviewDFX {

    typedef struct HiLogLabel {
        LogType type;
        unsigned int domain;
        const char *tag;
    } HiLogLabel;

    class HiLog final {
    public:
        static int Debug(const HiLogLabel &label, const char *fmt, ...);

        static int Info(const HiLogLabel &label, const char *fmt, ...);

        static int Warn(const HiLogLabel &label, const char *fmt, ...);

        static int Error(const HiLogLabel &label, const char *fmt, ...);

        static int Fatal(const HiLogLabel &label, const char *fmt, ...);
    };
}
#endif
