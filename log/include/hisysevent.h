#ifndef VIRTUAL_HIS_EVENT
#define VIRTUAL_HIS_EVENT

enum LogType {
	LOG_TYPE_MIN =0 ,
	LOG_INIT = 1,
	LOG_CORE = 3,
	LOG_TYPE_MAX
};

namespace OHOS::HiviewDFX {

class HiSysEvent final {
public:
    enum Domain {
        DISTRIBUTED_DATAMGR
    };
    enum EventType {
        FAULT = 1,
        STATISTIC = 2,
        SECURITY = 3,
        BEHAVIOR = 4
    };

    template<typename... Types>
	static void Write(Domain domain, std::string eventName, EventType type, Types... keyValues)
    {
    }
};
}
#endif
