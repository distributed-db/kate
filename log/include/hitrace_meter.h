#ifndef VIRTUAL_HI_TRACE_METER
#define VIRTUAL_HI_TRACE_METER

constexpr uint64_t HITRACE_TAG_DISTRIBUTEDDATA = 0;
void StartTrace(uint64_t tag, const std::string &action)
{
}

void FinishTrace(uint64_t tag)
{
}

void StartAsyncTrace(uint64_t tag, const std::string &action, int32_t taskId)
{
}

void FinishAsyncTrace(uint64_t tag, const std::string &action, int32_t taskId)
{
}
#endif
