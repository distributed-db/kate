#ifndef NATURALBASE_LITE_SCHEMA_INFO_H
#define NATURALBASE_LITE_SCHEMA_INFO_H

#include <stdio.h>

typedef struct NBSchemaInfo {
    unsigned char *data;
} NBSchemaInfo;

#define NB_COL_TYPE_TS      1
#define NB_COL_TYPE_INT64   2
#define NB_COL_TYPE_REAL    3
#define NB_COL_TYPE_TEXT    4
#define NB_COL_TYPE_BLOB    5

NBSchemaInfo *NB_CreateSchemaInfo(unsigned char *buff, size_t len)
{
    return NULL;
}

void NB_FreeSchemaInfo(NBSchemaInfo *schemaInfo)
{
    return;
}

int NB_TableCount(NBSchemaInfo *schemaInfo)
{
    return 0;
}

char *NB_TableName(NBSchemaInfo *schemaInfo, int tableIndex)
{
    return NULL;
}

int NB_TableID(NBSchemaInfo *schemaInfo, int tableIndex)
{
    return 0;
}

int NB_ColCount(NBSchemaInfo *schemaInfo, int tableIndex)
{
    return 0;
}

char *NB_ColName(NBSchemaInfo *schemaInfo, int tableIndex, int fieldIndex)
{
    return NULL;
}

int NB_ColDecltype(NBSchemaInfo *schemaInfo, int tableIndex, int fieldIndex)
{
    return NULL;
}

int NB_ColHasDefaultValue(NBSchemaInfo *schemaInfo, int tableIndex, int fieldIndex)
{
    return 0;
}

size_t NB_ColDefaultValue(NBSchemaInfo *schemaInfo, int tableIndex, int fieldIndex, char **value)
{
    value = NULL;
    return 0;
}

int NB_IsColNotNull(NBSchemaInfo *schemaInfo, int tableIndex, int fieldIndex)
{
    return 0;
}

int NB_IsColPrimaryKey(NBSchemaInfo *schemaInfo, int tableIndex, int fieldIndex)
{
    return 0;
}

int NB_GetSchemmaVer(NBSchemaInfo *schemaInfo)
{
    return 0;
}
#endif // NATURALBASE_LITE_SCHEMA_INFO_H