#include <hilog/log.h>
#include <cstdio>
#include <cstdarg>
#include <sys/timeb.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctime>
#include <iostream>

#define GET_TID gettid()

#define COLOR_ENDING       "\033[m"
#define RED_COLOR          "\033[0;32;31m"
#define LIGHT_RED_COLOR    "\033[1;31m"
#define YELLOW_COLOR       "\033[1;33m"

#if __GLIBC__ == 2 && __GLIBC_MINOR__ < 30
#include <sys/syscall.h>
#define gettid() syscall(__NR_gettid)
#endif

#define GET_TIME  log_Time()


namespace OHOS::HiviewDFX {
    static const int MAX_LOG_LENGTH = 1024;

    char *log_Time()
    {
        struct tm *ptm;
        time_t nowtime;
        static char szTime[19];
        struct timeval tv{};

        gettimeofday(&tv, nullptr);
        nowtime = tv.tv_usec;
        ptm = localtime(&tv.tv_sec);
        sprintf(szTime, "%02d-%02d %02d:%02d:%02d.%03d", ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min,
                ptm->tm_sec, (int) (nowtime % 1000));
        szTime[18] = 0;
        return szTime;
    }

    void GetLogHeaderAndTail(const std::string &level, std::string &colorHeader, std::string &colorTail)
    {
        colorHeader = "";
        colorTail = "";
        if (level == "WARN") {
            colorHeader = YELLOW_COLOR;
            colorTail = COLOR_ENDING;
        } else if (level == "ERROR") {
            colorHeader = LIGHT_RED_COLOR;
            colorTail = COLOR_ENDING;
        } else if (level == "FATAL") {
            colorHeader = RED_COLOR;
            colorTail = COLOR_ENDING;
        }
    }

#define LOG_PRINT(level) do { \
    char logBuff[MAX_LOG_LENGTH]; \
    int bytes; \
    va_list argList; \
    va_start(argList, fmt); \
    bytes = vsnprintf(logBuff, MAX_LOG_LENGTH, fmt, argList); \
    std::string colorHeader; \
    std::string colorTail; \
    GetLogHeaderAndTail(level, colorHeader, colorTail); \
    if (bytes < 0) { \
        printf("%s%5s: [%s]%s%s\n", colorHeader.c_str(), level, label.tag, "log too long", colorTail.c_str()); \
    } else { \
        printf("%s%5s: %s %8d: [%s]%s%s\n", colorHeader.c_str(), level, GET_TIME, GET_TID, label.tag, logBuff, colorTail.c_str()); \
    } \
    va_end(argList); \
} while (0)

    int HiLog::Debug(const HiLogLabel &label, const char *fmt, ...)
    {
        LOG_PRINT("DEBUG");
        return 0;
    }

    int HiLog::Info(const HiLogLabel &label, const char *fmt, ...)
    {
        LOG_PRINT("INFO");
        return 0;
    }

    int HiLog::Warn(const HiLogLabel &label, const char *fmt, ...)
    {
        LOG_PRINT("WARN");
        return 0;
    }

    int HiLog::Error(const HiLogLabel &label, const char *fmt, ...)
    {
        LOG_PRINT("ERROR");
        return 0;
    }

    int HiLog::Fatal(const HiLogLabel &label, const char *fmt, ...)
    {
        LOG_PRINT("FATAL");
        return 0;
    }
}
